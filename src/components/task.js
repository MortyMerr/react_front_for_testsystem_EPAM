import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Glyphicon, Panel} from "react-bootstrap";
import Log from "./log";
import {SUCCESSFUL_BUILD} from "../constants/logContants";

class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogShow: false
        }
    }

    logNeeded = taskInfo => taskInfo.log.buildResult === SUCCESSFUL_BUILD;

    render() {
        return (
            <div>
                {this.props.taskInfo.startTime}
                {this.logNeeded(this.props.taskInfo) ?
                    <Glyphicon bsStyle="text-success" glyph="ok"/> :
                    <Glyphicon onClick={() => this.setState({isLogShow: !this.state.isLogShow})} glyph="remove"/>
                }
                <Panel bsStyle={this.logNeeded(this.props.taskInfo) ? "success" : "danger"} collapsible expanded={this.state.isLogShow}>
                    <Log logInfo={this.props.taskInfo.log}/>
                </Panel>
            </div>
        )
    }
}

Task.propTypes = {
    taskInfo: PropTypes.shape({
        startTime: PropTypes.string.isRequired,
        log: PropTypes.object.isRequired,
        successful: PropTypes.bool.isRequired
    }).isRequired
};

export default Task;
