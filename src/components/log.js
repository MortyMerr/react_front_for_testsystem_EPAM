import React from 'react';
import PropTypes from 'prop-types'

import {
    Row,
    Col,
    Tab,
} from "react-bootstrap";
import Nav from "react-bootstrap/es/Nav";
import NavItem from "react-bootstrap/es/NavItem";


const Log = ({logInfo}) => {
    const cycles = [];
    for (let key in logInfo.cycles) {
        if (logInfo.cycles.hasOwnProperty(key)) {
            cycles.push({
                name: key,
                inf: logInfo.cycles[key].map((line, i) => <p>{line}</p>)
            });
        }
    }
    cycles.push({
        name: 'clones',
        inf: logInfo.clones.map((line, i) => <p>{line}</p>)
    });
    cycles.push({
        name: 'build result',
        inf: logInfo.buildResult
    });

    return (
        <Tab.Container defaultActiveKey={1}>
            <Row>
                <Col sm={4}>
                    <Nav stacked>
                        {cycles.map((cycle, i) => (<NavItem key={i} eventKey={i}> {cycle.name} </NavItem>))}
                    </Nav>
                </Col>
                <Col sm={8}>
                    <Tab.Content animation>
                        {cycles.map((cycle, i) => (<Tab.Pane key={i} eventKey={i}> {cycle.inf} </Tab.Pane>))}
                    </Tab.Content>
                </Col>
            </Row>
        </Tab.Container>
    )
};

Log.propTypes = {
    logInfo: PropTypes.shape({
        clones: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
        buildResult: PropTypes.string.isRequired,
        cycles: PropTypes.object.isRequired
    }).isRequired
};

export default Log;