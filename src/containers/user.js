import React, {Component} from 'react';
import PropTypes from 'prop-types'
import "bootswatch/journal/bootstrap.css";
import {Badge, ListGroup, ListGroupItem, Checkbox, Glyphicon, Panel} from "react-bootstrap";
import Task from "../components/task";

class User extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            isTasksShow: false
        }
    }

    render() {
        console.log(this.props.user.tasks);
        return (
            <div>
                <div onClick={() => this.setState({isTasksShow: !this.state.isTasksShow})}>
                    {this.props.user.gitNick}
                </div>
                <Panel collapsible expanded={this.state.isTasksShow}>
                    {this.props.user.tasks.map((task, i) => (
                        <Task taskInfo={task}/>
                    ))}
                </Panel>
            </div>
        )
    }
}

User.propTypes = {
    user: PropTypes.shape({
        email: PropTypes.string.isRequired,
        gitNick: PropTypes.string.isRequired,
        tasks: PropTypes.array.isRequired
    })
    //TODO add unresolved tasks count
};

export default User;
