import React, {Component} from 'react';
import fetch from 'isomorphic-fetch';
import {Badge, ListGroup, ListGroupItem, Checkbox, Glyphicon} from "react-bootstrap";
import {URL_USER} from "./constants/url";
import User from "./containers/user";

const initial_state = [];

class App extends Component {
    constructor() {
        super();
        this.state = {
            users: initial_state
        }
    }

    componentDidMount() {
        let self = this;
        fetch(URL_USER)
            .then(response => response.text())
            .then(response => {
                self.setState({
                    users: JSON.parse(response)
                });
            });
    }

    render() {
        return (
            <ListGroup>
                {this.state.users.map((user, i) => (
                    <ListGroupItem key={i}>
                        <User user={user}/>
                    </ListGroupItem>))
                }
            </ListGroup>
        )
    }

    /*return (
        <ListGroup>
            <ListGroupItem>
                <Task taskInfo={test_task}/>
            </ListGroupItem>
            <ListGroupItem>
                <Task taskInfo={test_task2}/>
            </ListGroupItem>
        </ListGroup>
    )
}*/
}

export default App;
